<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comments;
use App\Post;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $comments = Comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar comments berhasil ditampilkan',
            'data' => $comments,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = Comments::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Failed to Save Comments',
        ], 409);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $comments = Comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $comments 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comments $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comments::findOrFail($comment->id);

        if($comment) {

            //update post
            $comment->update([
                'content'     => $request->content,
                'post_id'   => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $comment  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $comments = Comments::findOrfail($id);

        if($comments) {

            //delete post
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comments Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comments not Found',
        ], 404);
    }
    
}
