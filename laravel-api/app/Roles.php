<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = ['nama'];

    protected $keyType = 'string';

    public $incrementing = false;

    public function user(){
        return $this->belongsTo('\App\Users', 'role_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->id = Str::uuid();
            }
        });
    }
}
