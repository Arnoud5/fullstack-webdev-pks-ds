<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title','description'];

    protected $keyType = 'string';

    public $incrementing = false;

    public function Comments(){
        return $this->belongsTo('\App\Comments', 'post_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->id = Str::uuid();
            }
        });
    }
}
