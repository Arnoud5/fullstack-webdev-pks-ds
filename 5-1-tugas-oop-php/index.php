<?php


abstract class Hewan extends Fight{
    // prop nama, darah =>50, jumlah kaki, keahlian
    public $nama;
    public $jumlahKaki;
    public $keahlian;
    public $darah=50;

    public function atraksi(){
        echo $this->nama." sedang ".$this->keahlian;
    }
    abstract public function getInfoHewan();
    
}

abstract class Fight{
    // attack power dan defence power
    public $atPower;
    public $dfPower;

    public function __construct($nama, $jumlahKaki, $keahlian, $atPower, $dfPower) {
        $this->nama = $nama;
        $this->jumlahKaki =$jumlahKaki;
        $this->keahlian =$keahlian;
        $this->attPower = $atPower;
        $this->dfPower = $dfPower;
        $this->darah;
    }

    public function diserang($nama1){
        echo $nama1." sedang diserang. Darah sekarang sisa ".$this->darah - $atPower/$this->dfPower;
    }

    public function serang($nama1){
        echo $this->nama." menyerang ".$nama1;
    }
}


// child class
class Elang extends Hewan{
    // Ketika Elang diinstansiasi, maka jumlahKaki bernilai 2, dan keahlian bernilai "terbang tinggi", attackPower = 10 , deffencePower = 5 
    public function getInfoHewan() : string {
        return nl2br("===============
        Nama Hewan : {$this->nama}
        Jumlah Kaki : {$this->jumlahKaki}
        Keahlian : {$this->jumlahKaki}
        Darah : {$this->darah}
        Attack Power : {$this->atPower}
        Defense Power : {$this->dfPower}
        ===============");
    }
}

class Harimau extends Hewan{
    // Ketika Harimau diintansiasi, maka jumlahKaki bernilai 4, dan keahlian bernilai "lari cepat" , attackPower = 7 , deffencePower = 8 ;
    public function getInfoHewan() : string {
        return nl2br("=========
        Nama Hewan : {$this->nama}
        Jumlah Kaki : {$this->jumlahKaki}
        Keahlian : {$this->jumlahKaki}
        Darah : {$this->darah}
        Attack Power : {$this->atPower}
        Defense Power : {$this->dfPower}
        ");
    }
}

                //  jmlh kaki = 2,  keahlian terbang tinggi, att=10, dfc=5
$elang = new Elang("Elang", 2,"terbang Tinggi", 10,5);
echo $elang->getInfoHewan();
echo "<br>";
echo $elang->atraksi();
echo "<br>";
echo $elang->serang($harimau->nama);
echo "<br>";
echo $elang->diserang($harimau->nama);
echo "<br>";
echo "<br>";
$harimau = new Harimau("harimau", 4,"Berlari cepat", 7,8);
echo $harimau->getInfoHewan();
echo "<br>";
echo $harimau->atraksi();
echo "<br>";
echo $harimau->serang("harimau");
echo "<br>";