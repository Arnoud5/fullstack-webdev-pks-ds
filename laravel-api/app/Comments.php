<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $fillable = ['content', 'post_id'];

    protected $keyType = 'string';

    public $incrementing = false;

    public function post(){
        return $this->hasOne('\App\Post', 'post_id');
    }


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->id = Str::uuid();
            }
        });
    }
}
